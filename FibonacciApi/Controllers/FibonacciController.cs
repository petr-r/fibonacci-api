﻿using System;
using System.Net;
using System.Web.Http;

namespace FibonacciApi.Controllers
{
    public class FibonacciController : ApiController
    {
        [Route("fibonacci/{n}")]
        public IHttpActionResult Get(int n)
        {
            var a = 0;
            var b = 1;

            for (var i = 0; i < n; i++)
            {
                var temp = a;
                a = b;
                b = temp + b;
            }

            return Content(HttpStatusCode.OK, a);
        }
    }
}